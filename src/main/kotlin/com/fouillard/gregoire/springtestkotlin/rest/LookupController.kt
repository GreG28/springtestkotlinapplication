package com.fouillard.gregoire.springtestkotlin.rest

import com.fouillard.gregoire.springtestkotlin.data.Lookup
import com.fouillard.gregoire.springtestkotlin.exception.StorageException
import com.fouillard.gregoire.springtestkotlin.service.LookupService
import com.fouillard.gregoire.springtestkotlin.service.StorageService
import org.apache.poi.UnsupportedFileFormatException
import org.apache.poi.util.IOUtils
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.io.FileNotFoundException
import java.io.IOException


@RestController
@RequestMapping("/v1")
class LookupController(val lookupService: LookupService, val storageService: StorageService) {

    @GetMapping("/lookup/{key}")
    fun getLookupByKey(@PathVariable key: String): List<Lookup>? {
        return lookupService.getByKey(key)
    }

    @GetMapping("/lookups")
    fun getLookups(): MutableMap<String, ArrayList<Lookup>> {
        return lookupService.getAll()
    }

    @PostMapping("/lookupFile")
    fun uploadLookupFile(@RequestParam("file") file: MultipartFile,
                         redirectAttributes: RedirectAttributes): String {
        val filenameStored = storageService.store(file)
        var isValid = false
        try {
            lookupService.loadLookupFile(storageService.loadAsResource(filenameStored))
            isValid = true
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            storageService.delete(filenameStored)
        } catch (e: IOException) {
            e.printStackTrace()
            storageService.delete(filenameStored)
        } catch (e: UnsupportedFileFormatException) {
            e.printStackTrace()
            storageService.delete(filenameStored)
        } catch (e: IllegalStateException) {
            e.printStackTrace()
            storageService.delete(filenameStored)
        }
        return if (isValid) {
            "$filenameStored is valid and stored"
        } else {
            "$filenameStored is not valid and not stored"
        }
    }

    @GetMapping(value = ["/lookupFile/lastModified"], produces = arrayOf(MediaType.APPLICATION_OCTET_STREAM_VALUE))
    @ResponseBody
    @Throws(StorageException::class)
    fun getLastModifiedLookupFile(): ByteArray {
        try {
            return IOUtils.toByteArray(storageService.load(storageService.getFilenameOfLastModified()!!).toFile().inputStream())
        } catch (e: IOException) {
            throw StorageException("Problem getting lastModifiedFile", e)
        }
    }


    @GetMapping("/lookupFile/lastModified/filename")
    fun getLastModifiedLookupFileName(): String? {
        return storageService.getFilenameOfLastModified()
    }

    @GetMapping(value = ["/lookupFile/{filename}"], produces = arrayOf(MediaType.APPLICATION_OCTET_STREAM_VALUE))
    @ResponseBody
    @Throws(StorageException::class)
    fun getLookupFileByName(@PathVariable("filename") filename: String): ByteArray {
        if (storageService.exist(filename)) {
            return IOUtils.toByteArray(storageService.load(filename).toFile().inputStream())
        } else {
            throw StorageException("$filename does ot exist")
        }
    }

    @GetMapping("/lookupFiles")
    fun getLookupFilesName(): List<String> {
        return storageService.loadAll()
    }

}