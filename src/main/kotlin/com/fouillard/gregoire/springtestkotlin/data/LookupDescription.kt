package com.fouillard.gregoire.springtestkotlin.data

data class LookupDescription(val langage: String, val description: String)