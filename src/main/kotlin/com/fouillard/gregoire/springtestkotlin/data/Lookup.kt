package com.fouillard.gregoire.springtestkotlin.data

data class Lookup(val key: String, val descriptions: ArrayList<LookupDescription>)