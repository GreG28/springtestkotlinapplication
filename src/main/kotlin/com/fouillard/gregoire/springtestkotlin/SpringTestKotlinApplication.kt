package com.fouillard.gregoire.springtestkotlin

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringTestKotlinApplication

fun main(args: Array<String>) {
    SpringApplication.run(SpringTestKotlinApplication::class.java, *args)
}
