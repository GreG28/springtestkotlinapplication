package com.fouillard.gregoire.springtestkotlin.service.impl

import com.fouillard.gregoire.springtestkotlin.data.Lookup
import com.fouillard.gregoire.springtestkotlin.data.LookupDescription
import com.fouillard.gregoire.springtestkotlin.exception.LookupNotFoundException
import com.fouillard.gregoire.springtestkotlin.service.LookupService
import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Service
import java.util.logging.Logger


/**
 * Exemple for loading file from resource directory
 *
 * https://smarterco.de/java-load-file-from-classpath-in-spring-boot/
 *
 */
@Service
class LookupServiceImpl : LookupService {

    private final val LANGAGE_EN = "EN"
    private final val LANGAGE_FR = "FR"

    private final val LOGGER: Logger = Logger.getLogger(LookupServiceImpl::class.simpleName)

    private final val repo: MutableMap<String, ArrayList<Lookup>> = mutableMapOf()

    private val lookupFilepath: String
    private val resourceLoader: ResourceLoader

    @Autowired
    constructor(@Value("classpath:\${com.lookup.file}") lookupFilepath: String, resourceLoader: ResourceLoader) {
        this.lookupFilepath = lookupFilepath
        this.resourceLoader = resourceLoader

        // Loading of file from the projet resource
        this.loadLookupFile(getLookupResource())
    }

    final override fun getLookupResource(): Resource {
        return resourceLoader.getResource(lookupFilepath)
    }

    final override fun loadLookupFile(lookupFile: Resource) {

        LOGGER.info("filePath " + lookupFilepath)

        val newRepo: MutableMap<String, ArrayList<Lookup>> = mutableMapOf()

        val excelInputStream = lookupFile.inputStream
        val workbook = XSSFWorkbook(excelInputStream)
        val datatypeSheet = workbook.getSheet("CodeListValues")
        val iterator = datatypeSheet.iterator()
        val formatter = DataFormatter()

        // going to 2nd row
        iterator.next()

        while (iterator.hasNext()) {

            val currentRow = iterator.next()

            val lookupKey = currentRow.getCell(1)

            if (formatter.formatCellValue(lookupKey).isNullOrEmpty()) {
                LOGGER.info("row ignored " + currentRow.rowNum)
                continue
            }

            var lookups: ArrayList<Lookup> = ArrayList()
            if (newRepo.containsKey(formatter.formatCellValue(lookupKey).trim())) {
                lookups = newRepo[formatter.formatCellValue(lookupKey).trim()]!!
            }

            val lookupCode = currentRow.getCell(2)
            val lookupEN = currentRow.getCell(7)
            val lookupFR = currentRow.getCell(8)

            val descriptionEN = LookupDescription(LANGAGE_EN, formatter.formatCellValue(lookupEN).trim())
            val descriptionFR = LookupDescription(LANGAGE_FR, formatter.formatCellValue(lookupFR).trim())
            lookups.add(Lookup(formatter.formatCellValue(lookupCode).trim(), arrayListOf(descriptionEN, descriptionFR)))

            newRepo[formatter.formatCellValue(lookupKey).trim()] = lookups
        }

        repo.clear()
        repo.putAll(newRepo)
    }

    override fun getByKey(key: String): List<Lookup>? {
        if (!repo.containsKey(key)) {
            throw LookupNotFoundException("""Lookup with key : $key not found""")
        } else {
            return repo.getValue(key)
        }
    }

    override fun getAll(): MutableMap<String, ArrayList<Lookup>> {
        return repo
    }
}