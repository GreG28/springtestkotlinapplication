package com.fouillard.gregoire.springtestkotlin.service

import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Path


interface StorageService {

    fun init()

    fun store(file: MultipartFile): String

    fun loadAll(): List<String>

    fun getFilenameOfLastModified(): String?

    fun exist(filename: String): Boolean

    fun load(filename: String): Path

    fun loadAsResource(filename: String): Resource

    fun delete(filename: String)

    fun deleteAll()

    fun getFilenameWithTimestamp(filename: String): String

}