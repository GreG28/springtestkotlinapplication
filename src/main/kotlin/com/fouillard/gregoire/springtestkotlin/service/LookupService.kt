package com.fouillard.gregoire.springtestkotlin.service

import com.fouillard.gregoire.springtestkotlin.data.Lookup
import com.fouillard.gregoire.springtestkotlin.exception.LookupNotFoundException
import org.apache.poi.UnsupportedFileFormatException
import org.springframework.core.io.Resource
import java.io.FileNotFoundException
import java.io.IOException

interface LookupService {

    fun getLookupResource(): Resource?

    @Throws(FileNotFoundException::class, IOException::class, UnsupportedFileFormatException::class, IllegalStateException::class)
    fun loadLookupFile(lookupFile: Resource)

    @Throws(LookupNotFoundException::class)
    fun getByKey(key: String): List<Lookup>?

    fun getAll(): MutableMap<String, ArrayList<Lookup>>

}