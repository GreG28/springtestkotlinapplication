package com.fouillard.gregoire.springtestkotlin.service.impl

import com.fouillard.gregoire.springtestkotlin.config.StorageProperties
import com.fouillard.gregoire.springtestkotlin.exception.StorageException
import com.fouillard.gregoire.springtestkotlin.exception.StorageFileNotFoundException
import com.fouillard.gregoire.springtestkotlin.service.StorageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.util.FileSystemUtils
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.stream.Collectors


@Service
class FileSystemStorageServiceImpl @Autowired
constructor(properties: StorageProperties) : StorageService {

    private val rootLocation: Path = Paths.get(properties.uploadlocation)

    override fun store(file: MultipartFile): String {
        try {
            val filename = getFilenameWithTimestamp(file.originalFilename)
            if (file.isEmpty) {
                throw StorageException("Failed to store empty file $filename")
            }
            Files.copy(file.inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING)
            return filename
        } catch (e: IOException) {
            throw StorageException("Failed to store file " + file.originalFilename, e)
        }
    }

    override fun loadAll(): List<String> {
        try {
            return Files.list(this.rootLocation)
                    .map { path -> path.toFile() }
                    .sorted({ file1, file2 -> file1.lastModified().compareTo(file2.lastModified()) })
                    .map { file -> file.name }
                    .collect(Collectors.toList())
        } catch (e: IOException) {
            throw StorageException("Failed to read stored files", e)
        }
    }

    override fun getFilenameOfLastModified(): String? {
        try {
            return Files.newDirectoryStream(this.rootLocation, { path -> path.toFile().isFile })
                    .map { path -> path.toFile() }
                    .stream()
                    .sorted { file1, file2 -> file1.lastModified().compareTo(file2.lastModified()) }
                    .map { file -> file.name }
                    .collect(Collectors.toList())
                    .lastOrNull()
        } catch (e: IOException) {
            throw StorageException("Failed to read stored files", e)
        }
    }

    override fun load(filename: String): Path {
        return rootLocation.resolve(filename)
    }

    override fun loadAsResource(filename: String): Resource {
        try {
            val file = load(filename)
            val resource = UrlResource(file.toUri())
            return if (resource.exists() || resource.isReadable) {
                resource
            } else {
                throw StorageFileNotFoundException("Could not read file: $filename")

            }
        } catch (e: MalformedURLException) {
            throw StorageFileNotFoundException("Could not read file: $filename", e)
        }
    }

    override fun delete(filename: String) {
        Files.deleteIfExists(rootLocation.resolve(filename))
    }

    override fun deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile())
    }

    override fun getFilenameWithTimestamp(filename: String): String {
        return "${LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)}-${filename.decapitalize().filterNot { c -> c.isWhitespace() }}"
    }

    override fun exist(filename: String): Boolean {
        return Files.exists(this.rootLocation.resolve(filename))
    }

    override fun init() {
        try {
            Files.createDirectory(rootLocation)
        } catch (e: IOException) {
            throw StorageException("Could not initialize storage", e)
        }

    }
}