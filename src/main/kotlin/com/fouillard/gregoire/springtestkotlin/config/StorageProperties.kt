package com.fouillard.gregoire.springtestkotlin.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "spring-test-kotlin-storage")
class StorageProperties(@Value("uploadlocation") var uploadlocation: String)