package com.fouillard.gregoire.springtestkotlin.exception.response

data class ErrorResponse(val statusCode: Int, val statusMessage: String, val message: String)