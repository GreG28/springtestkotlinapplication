package com.fouillard.gregoire.springtestkotlin.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.I_AM_A_TEAPOT, reason = "Lookup not found")
class LookupNotFoundException(message: String): RuntimeException(message)
