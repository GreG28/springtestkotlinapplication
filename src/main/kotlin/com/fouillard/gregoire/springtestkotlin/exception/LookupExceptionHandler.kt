package com.fouillard.gregoire.springtestkotlin.exception

import com.fouillard.gregoire.springtestkotlin.exception.response.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody

@ControllerAdvice
class LookupExceptionHandler {

    @ExceptionHandler(LookupNotFoundException::class)
    @ResponseBody
    fun onLookupException(ex: LookupNotFoundException): ResponseEntity<ErrorResponse> {
        val httpError = HttpStatus.BAD_REQUEST
        val errorResponse = ErrorResponse(httpError.value(), httpError.reasonPhrase, ex.message
                ?: "Bad request")
        return ResponseEntity(errorResponse, httpError)
    }

    @ExceptionHandler(StorageException::class, StorageFileNotFoundException::class)
    @ResponseBody
    fun onStorageException(ex: StorageException): ResponseEntity<ErrorResponse> {
        val httpError = HttpStatus.BAD_REQUEST
        val errorResponse = ErrorResponse(httpError.value(), httpError.reasonPhrase, ex.message
                ?: "Bad request")
        return ResponseEntity(errorResponse, httpError)
    }
}