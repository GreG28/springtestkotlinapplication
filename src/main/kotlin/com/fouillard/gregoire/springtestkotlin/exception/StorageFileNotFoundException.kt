package com.fouillard.gregoire.springtestkotlin.exception

import com.fouillard.gregoire.springtestkotlin.exception.StorageException

class StorageFileNotFoundException : StorageException {

    constructor(message: String) : super(message)

    constructor(message: String, cause: Throwable) : super(message, cause)
}