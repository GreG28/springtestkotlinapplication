# SpringTestKotlinApplication

Test pour voir comment fonctionne la syntaxe de Kotlin et comment faire une API rest avec Spring Boot dans ce language.


## Swagger : 

[Url du Swagger](http://localhost:8080/swagger-ui.html)


## Urls de test - Lookup : 

[Obtenir la liste des lookups par clé "ActivityCode"](http://localhost:8080/v1/lookup/ActivityCode)

### Cas de catch pour les exceptions lors d'un appel a un lookup inexistant 

[Url non fonctionnelle](http://localhost:8080/v1/lookup/plop)

## Url pour obtenir l'ensemble des lookups  : 

[Obtenir la liste des lookups](http://localhost:8080/v1/lookups)

## Url pour uploader un nouveau fichier de lookups : 

[Url d'upload d'un fichier de lookups](http://localhost:8080/v1/uploadLookupFile)

## Gradle

### Build Jar

Execute the following command to construct jar of our project

```bash
./gradlew build
```

The constructed jar can be found inside the following directory

```bash
./build/libs/spring-test-kotlin-1.0.0-SNAPSHOT.jar
```

# Link to resources 

## Uploading file with spring

[Uploading Files example](https://spring.io/guides/gs/uploading-files/)

